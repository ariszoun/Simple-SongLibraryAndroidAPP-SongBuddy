# SIMPLE SONG LIBRARY ANDROID EDUCATIONAL APPLICATION "SongBuddy"

## This application is made for educational purposes concerning a simple library-type management application (entry level) that:

- CRUD operations are implemented using SQLite database
- Recycler view is implemented

The RecyclerView widget is a more advanced and flexible version of ListView. 
https://developer.android.com/guide/topics/ui/layout/recyclerview
  